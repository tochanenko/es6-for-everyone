import { callApi } from '../helpers/apiHelper';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');

    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id) {
  try {
    // i will never that ` is not the same as ', however ' and " are.
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    return apiResult;
  } catch (error) {
    console.log(error);
    throw error;
  }
  // endpoint - `details/fighter/${id}.json`;
}

