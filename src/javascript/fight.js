export function fight(firstFighter, secondFighter) {
  while (true) {
    if (firstFighter.health > 0) {
      secondFighter.health -= getDamage(firstFighter, secondFighter);
    } else {
      return secondFighter;
    }

    if (secondFighter.health > 0) {
      firstFighter.health -= getDamage(secondFighter, firstFighter);
    } else {
      return firstFighter;
    }
  }
}

export function getDamage(attacker, enemy) {
  return Math.max(0, getHitPower(attacker) - getBlockPower(enemy));
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}
