import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  console.log(fighter);
  const { name, health, source } = fighter;

  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const healthLeft = createElement({ tagName: 'div', className: 'health-attack' });
  const imageElement = createElement({ tagName: 'img', className: 'image-attack' });

  const title = name + ' winner!';

  healthLeft.innerText = `Left ♥: ${health}`;
  imageElement.src = source;

  bodyElement.append(healthLeft);
  bodyElement.append(imageElement);

  console.log(title);
  console.log(bodyElement);

  showModal({ title, bodyElement });
}
